//
//  SizeModel.swift
//  PhotosApp
//
//  Created by Jun on 4/13/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SwiftyJSON

class SizeModel: NSObject {
    var label: String = ""
    var source: String = ""
    
    func importJsonData(data: JSON) {
        if let label = data["label"].string {
            self.label = label
        }
        
        if let source = data["source"].string {
            self.source = source
        }
    }
}


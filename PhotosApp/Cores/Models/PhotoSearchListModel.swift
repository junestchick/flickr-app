//
//  PhotoSearchListModel.swift
//  PhotosApp
//
//  Created by Jun on 4/17/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SwiftyJSON

class PhotoSearchListModel: NSObject {
    var page: Int = 0
    var pages: Int = 0
    var total: Int = 0
    var stat: String = ""
    var photos: Array<PhotoModel> = Array<PhotoModel>()
    
    func importJsonData(data: JSON) {
        if let stat = data["stat"].string {
            self.stat = stat
        }
        guard let photosets = data["photos"].dictionary else { return }
        
        if let page = photosets["page"]?.intValue {
            self.page = page
        }
        
        if let pages = photosets["pages"]?.intValue {
            self.pages = pages
        }
        
        if let total = photosets["total"]?.intValue {
            self.total = total
        }
        
        if let photosArray = photosets["photo"]?.array {
            for item in photosArray {
                let photo = PhotoModel()
                photo.importJsonData(data: item)
                photos.append(photo)
            }
        }
    }
}

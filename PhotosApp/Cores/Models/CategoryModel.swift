//
//  CategoryModel.swift
//  PhotosApp
//
//  Created by Jun on 4/12/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryModel: NSObject {
    var id: String = ""
    var primary: String = ""
    var secret: String = ""
    var server: String = ""
    var farm: Int = 0
    var title: String = ""
    var des: String = ""
    var photos: String = ""
    
    func importJsonData(data: JSON) {
        if let id = data["id"].string {
            self.id = id
        }
        
        if let primary = data["primary"].string {
            self.primary = primary
        }
        
        if let secret = data["secret"].string {
            self.secret = secret
        }
        
        if let server = data["server"].string {
            self.server = server
        }
        
        if let farm = data["farm"].int {
            self.farm = farm
        }
        
        if let titleDic = data["title"].dictionary {
            if let title = titleDic["_content"]?.stringValue {
                self.title = title
            }
        }
        
        if let desDic = data["description"].dictionary {
            if let des = desDic["_content"]?.stringValue {
                self.des = des
            }
        }
        
        if let photos = data["photos"].string {
            self.photos = photos
        }
    }
}

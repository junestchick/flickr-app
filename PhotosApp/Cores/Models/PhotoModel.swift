//
//  PhotoModel.swift
//  PhotosApp
//
//  Created by Jun on 4/12/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SwiftyJSON

class PhotoModel: NSObject {
    var id: String = ""
    var secret: String = ""
    var server: String = ""
    var farm: Int = 0
    var title: String = ""
    
    func importJsonData(data: JSON) {
        if let id = data["id"].string {
            self.id = id
        }
        
        if let secret = data["secret"].string {
            self.secret = secret
        }
        
        if let server = data["server"].string {
            self.server = server
        }
        
        if let farm = data["farm"].int {
            self.farm = farm
        }
        
        if let title = data["title"].string {
            self.title = title
        }
    }
}

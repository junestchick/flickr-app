//
//  SizeListModel.swift
//  PhotosApp
//
//  Created by Jun on 4/13/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SwiftyJSON

class SizeListModel: NSObject {
    var page: Int = 0
    var pages: Int = 0
    var total: Int = 0
    var stat: String = ""
    var sizes: Array<SizeModel> = Array<SizeModel>()
    
    func importJsonData(data: JSON) {
        if let stat = data["stat"].string {
            self.stat = stat
        }
        guard let sizesDic = data["sizes"].dictionary else { return }
        
        if let sizeArray = sizesDic["size"]?.array {
            for item in sizeArray {
                let size = SizeModel()
                size.importJsonData(data: item)
                sizes.append(size)
            }
            
            
        }
    }
}

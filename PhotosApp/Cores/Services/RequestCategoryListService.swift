//
//  RequestCategoryListService.swift
//  PhotosApp
//
//  Created by Jun on 4/12/17.
//  Copyright © 2017 Junest. All rights reserved.
//

class RequestCategoryListService: FLService {
    func requestCategoryList(atPage page: Int, completion: FLDataCompletion?) {
        let pageUrl = String.init(format: "&page=%d", page)
        let url = ServiceURL.END_POINT + ServiceURL.GET_CATEGORY_LIST + ServiceURL.API_KEY_URL + ServiceURL.FLICKR_USER_ID_URL + pageUrl + ServiceURL.CATE_PER_PAGE_URL + ServiceURL.FORMAT_TYPE_URL
        request(method: .get, urlString: url, parameters: nil, headers: nil) { (data, error) in
            completion?(data, error)
        }
    }
}

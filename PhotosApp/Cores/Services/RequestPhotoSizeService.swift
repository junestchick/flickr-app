//
//  RequestPhotoSizeService.swift
//  PhotosApp
//
//  Created by Jun on 4/13/17.
//  Copyright © 2017 Junest. All rights reserved.
//

class RequestPhotoSizeService: FLService {
    func requestPhotoSize(photoID: String, completion: FLDataCompletion?) {
        let photoUrl = String.init(format: "&photo_id=%@", photoID)
        let url = ServiceURL.END_POINT + ServiceURL.GET_PHOTO_SIZE + ServiceURL.API_KEY_URL + photoUrl + ServiceURL.FLICKR_USER_ID_URL + ServiceURL.ITEMS_PER_PAGE_URL + ServiceURL.FORMAT_TYPE_URL
        print(url)
        request(method: .get, urlString: url, parameters: nil, headers: nil) { (data, error) in
            completion?(data, error)
        }
    }
}

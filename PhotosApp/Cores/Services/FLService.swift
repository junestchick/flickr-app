



import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD


typealias FLDataCompletion = (_ data: JSON?, _ error: NSError?) -> Void
typealias FLCompletion = (_ success: Bool, _ error: NSError?) -> Void
//typealias EVMultipartFormData = (multipartData: MultipartFormData) -> Void

class FLService: NSObject {
    func request(method: HTTPMethod,
                 urlString: URLConvertible,
                 parameters: [String: AnyObject]? = nil,
                 headers: [String: String]? = nil,
                 completion: FLDataCompletion?) {
        
        let rq = Alamofire.request(urlString, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        rq.responseData { (response) in
//            let error = self.parseError(response, error: nil)
            let data = self.parseJSONData(data: response.data)
            if let error = response.error {
                completion?(nil, error as NSError)
            } else {
                completion?(data, nil)
            }
        }
    }
    
    private func parseError(response: HTTPURLResponse?, error: NSError?) -> NSError? {
        var message = ""
        if let tempError = error, tempError.code == -1009 {
            message = "MESSAGE_ERROR_NETWORK"
            return NSError(domain: "COErrorDomain", code:0, userInfo: ["code": -1009, "message": message])
        } else {
            if let responseCode = response?.statusCode, responseCode >= 400 {
                if error != nil {
                    message = (error?.description)!
                }
                return NSError(domain: "COErrorDomain", code:0, userInfo: ["code": 500, "message": message])
            } else {
                return error
            }
        }
    }
    
    private func parseJSONData(data: Data?) -> JSON? {
        if data != nil {
            return JSON(data: data! as Data)
        } else {
            return nil
        }
    }
    
}

extension JSON {
    public func date(dateFormatter: DateFormatter) -> NSDate? {
        if let string = self.string {
            if let date = dateFormatter.date(from: string) {
                return date as NSDate
            }
        }
        return nil
    }
}

//
//  RequestSearchPhotoListService.swift
//  PhotosApp
//
//  Created by Jun on 4/17/17.
//  Copyright © 2017 Junest. All rights reserved.
//

class RequestSearchPhotoListService: FLService {
    func requestSearchPhoto(keyword: String, atPage page: Int, completion: FLDataCompletion?) {
        let pageUrl = String.init(format: "&page=%d", page)
        let noSpaceKeyword = keyword.replacingOccurrences(of: " ", with: "+")
        let keywordUrl = String.init(format: "&text=%@", noSpaceKeyword)
        let url = ServiceURL.END_POINT + ServiceURL.KEYWORK_SEARCH + ServiceURL.API_KEY_URL + keywordUrl + ServiceURL.FLICKR_USER_ID_URL + pageUrl + ServiceURL.ITEMS_PER_PAGE_URL + ServiceURL.FORMAT_TYPE_URL
        print(url)
        request(method: .get, urlString: url, parameters: nil, headers: nil) { (data, error) in
            completion?(data, error)
        }
    }
}


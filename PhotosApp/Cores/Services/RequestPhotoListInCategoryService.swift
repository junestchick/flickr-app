//
//  RequestPhotoListInCategoryService.swift
//  PhotosApp
//
//  Created by Jun on 4/12/17.
//  Copyright © 2017 Junest. All rights reserved.
//

class RequestPhotoListInCategoryService: FLService {
    func requestPhotoListInCategory(categoryID: String, atPage page: Int, completion: FLDataCompletion?) {
        let pageUrl = String.init(format: "&page=%d", page)
        let categoryUrl = String.init(format: "&photoset_id=%@", categoryID)
        let url = ServiceURL.END_POINT + ServiceURL.GET_PHOTO_IN_CATEGORY + ServiceURL.API_KEY_URL + categoryUrl + ServiceURL.FLICKR_USER_ID_URL + pageUrl + ServiceURL.ITEMS_PER_PAGE_URL + ServiceURL.FORMAT_TYPE_URL
        print(url)
        request(method: .get, urlString: url, parameters: nil, headers: nil) { (data, error) in
            completion?(data, error)
        }
    }
}

//
//  UIHelper.swift
//  homework
//
//  Created by Dũng Trần on 12/6/16.
//  Copyright © 2016 Junest. All rights reserved.
//

import UIKit

typealias Completion = (() -> Void)
var AppIsShowingAlert = false
enum PhotoSize: String {
    case thumbnail = "t"
    case small = "m"
    case medium = "b"
    case original = "o"
}

class UIHelper: NSObject {
    class func showAlert(withMessage message: String, inViewController controller: UIViewController, completion: Completion?) {
        let alertVC = UIAlertController(title: "SmartDev", message: message, preferredStyle: .alert)
        let actionConfirm = UIAlertAction(title: "Close", style: .default) { (alert) in
            if let nonNilCompletion = completion {
                nonNilCompletion()
            }
        }
        alertVC.addAction(actionConfirm)
        controller.present(alertVC, animated: true, completion: nil)
    }
    
    class func setUIpropertyForView(view: UIView,
                                    borderColor: CGColor,
                                    borderWidth: CGFloat,
                                    cornerRadius: CGFloat) {
        view.layer.masksToBounds = true
        view.clipsToBounds = true
        view.layer.borderColor = borderColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
    }
    
    class func getPhotoLink(farm: Int, server: String, id: String, secret: String, size: PhotoSize) -> String {
        return String.init(format: "https://farm%d.staticflickr.com/%@/%@_%@_%@.jpg", arguments: [farm, server, id, secret, size.rawValue])
    }
    
    class func getPhotoLink(photoData: PhotoModel, size: PhotoSize) -> String {
        return String.init(format: "https://farm%d.staticflickr.com/%@/%@_%@_%@.jpg", arguments: [photoData.farm, photoData.server, photoData.id, photoData.secret, size.rawValue])
    }
}

extension UITextField {
    func addRadiusToConrners(corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect:self.bounds,
                                byRoundingCorners:corners,
                                cornerRadii: CGSize(width: 7, height:  7))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

extension UISearchBar {
    public func setTextColor(color: UIColor) {
        let svs = subviews.flatMap { $0.subviews }
        guard let tf = (svs.filter { $0 is UITextField }).first as? UITextField else { return }
        tf.textColor = color
    }
}

class NewTextField: UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10 , dy: 0)
    }
}

let TMImageZoom_Started_Zoom_Notification = "TMImageZoom_Started_Zoom_Notification"
let TMImageZoom_Ended_Zoom_Notification = "TMImageZoom_Ended_Zoom_Notification"

class InstaImageZoom {
    var isHandlingGesture =  false
    static let shared = InstaImageZoom()
    private var currentImageView = UIImageView()
    private var hostImageView = UIImageView()
    private var isAnimatingReset = false
    private var firstCenterPoint = CGPoint.zero
    private var  startingRect = CGRect.zero
    
    func gestureStateChanged(gesture: UIPinchGestureRecognizer, withZoomImageView imageView: UIImageView) {
        let theGesture = gesture
        if isAnimatingReset {
            return
        }
        if theGesture.state == .ended || theGesture.state == .cancelled || theGesture.state == .failed {
            self.resetImageZoom()
            return
        }
        if isHandlingGesture && hostImageView != imageView {
            return
        }
        if !isHandlingGesture && theGesture.state == .began {
            isHandlingGesture = true
            hostImageView = imageView
            imageView.isHidden = true
            let point = imageView.convert(imageView.frame.origin, to: nil)
            startingRect = CGRect(x: point.x, y: point.y, width: imageView.frame.size.width, height: imageView.frame.size.height)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: TMImageZoom_Started_Zoom_Notification), object: nil)
            
            guard let currentWindow = UIApplication.shared.keyWindow else { return }
            
            firstCenterPoint = theGesture.location(in: currentWindow)
            
            currentImageView = UIImageView(image: imageView.image)
            currentImageView.contentMode = imageView.contentMode
            // Init zoom ImageView
            currentImageView.frame = startingRect
            currentWindow.addSubview(currentImageView)
        }
        if theGesture.numberOfTouches < 2 {
            self.resetImageZoom()
            return
        }
        
        if theGesture.state == .changed {
            if startingRect.size.width == 0 { return }
            let currentScale = currentImageView.frame.size.width / startingRect.size.width
            let newScale = currentScale * theGesture.scale
            currentImageView.frame = CGRect(x: currentImageView.frame.origin.x,
                                            y: currentImageView.frame.origin.y,
                                            width: startingRect.size.width * newScale,
                                            height: startingRect.size.height * newScale)
            guard let currentWindow = UIApplication.shared.keyWindow else { return }
            let centerXDif = firstCenterPoint.x - theGesture.location(in: currentWindow).x
            let centerYDif = firstCenterPoint.y - theGesture.location(in: currentWindow).y
            currentImageView.center = CGPoint(x: (startingRect.origin.x + (startingRect.size.width / 2)) - centerXDif, y: (startingRect.origin.y + (startingRect.size.height / 2)) - centerYDif)
            theGesture.scale = 1
        }
    }
    
    func resetImageZoom() {
        if isAnimatingReset || !isHandlingGesture {
            return
        }
        
        isAnimatingReset = true
        UIView.animate(withDuration: 0.2, animations: { 
            self.currentImageView.frame = self.startingRect
        }) { (finished) in
            self.currentImageView.removeFromSuperview()
            self.currentImageView = UIImageView()
            self.hostImageView.isHidden = false
            self.hostImageView = UIImageView()
            self.startingRect = CGRect.zero
            self.firstCenterPoint = CGPoint.zero
            self.isHandlingGesture = false
            self.isAnimatingReset = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: TMImageZoom_Ended_Zoom_Notification), object: nil)
        }
    }
}








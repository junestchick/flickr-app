//
//  Defines.swift
//  PhotosApp
//
//  Created by Jun on 4/12/17.
//  Copyright © 2017 Junest. All rights reserved.
//
import UIKit

struct GoogleSDKKey {
    //Google admob app id and ad unit id
    static let APP_ID = "ca-app-pub-6474664831259018~9342342729"
    static let AD_UNIT_ID = "ca-app-pub-6474664831259018/1819075921"
    static let TEST_AD_ID = "ca-app-pub-3940256099942544/1033173712"
}

struct ServiceKey {
    //Flickr user id and flickr api key
    static let FLICKR_USER_ID = "68177032%40N04"
    static let API_KEY = "2231bbeda332144d94001d44f1ea6489"
}

struct AppString {
    //App name, show in top of left menu and top of home.
    static let APP_NAME = "Flickr App"
}

struct AppSegments {
    // Create 3 segments of home screen with name and id of photoset
    static let FIRST = Segment(NAME: "RECENT", ID: "72157644054649881")
    static let SECOND = Segment(NAME: "SHUFFLE", ID: "72157663044572194")
    static let THIRD = Segment(NAME: "FEATURE", ID: "72157644358431566")
}

struct AppColor {
    static let MAIN_COLOR = UIColor(rgb: 0x0D4F78)
}

struct GlobalConst {
    // Number of item per request
    static let NUMBER_OF_ITEMS_PER_PAGE = 10
    static let NUMBER_OF_CATE_PER_PAGE = 99
    /* Number of columns in photo list view:
     Currently: ipad pro    : 5
                ipad        : 4
                iphone plus : 3
                default     : 2
     */
    static let NUMBER_OF_COLUMNS = DeviceType.IS_IPAD_PRO ? 5 : DeviceType.IS_IPAD ? 4 : DeviceType.IS_IPHONE_6P ? 3 : 2
}

struct ServiceURL {
    
    // COMMON URL
    static let END_POINT            =   "https://api.flickr.com/services/rest/?method="
    static let FORMAT_TYPE_URL      =   "&format=json&nojsoncallback=1"
    static let FLICKR_USER_ID_URL   =   String.init(format: "&user_id=%@", ServiceKey.FLICKR_USER_ID)
    static let API_KEY_URL          =   String.init(format: "&api_key=%@", ServiceKey.API_KEY)
    static let ITEMS_PER_PAGE_URL   =   String.init(format: "&per_page=%d", GlobalConst.NUMBER_OF_ITEMS_PER_PAGE)
    static let CATE_PER_PAGE_URL   =   String.init(format: "&per_page=%d", GlobalConst.NUMBER_OF_CATE_PER_PAGE)
    
    // METHOD TYPE
    static let GET_CATEGORY_LIST        =   "flickr.photosets.getList"
    static let GET_PHOTO_IN_CATEGORY    =   "flickr.photosets.getPhotos"
    static let GET_PHOTO_SIZE           =   "flickr.photos.getSizes"
    static let KEYWORK_SEARCH           =   "flickr.photos.search"
    
}

enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}

struct Segment {
    var NAME: String
    var ID: String
}




//
//  PagedPhotoDetailCollectionCell.swift
//  PhotosApp
//
//  Created by Jun on 4/13/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import SVProgressHUD
import UIKit
import KCFloatingActionButton
import FCAlertView
import Photos

class PagedPhotoDetailCollectionCell: UICollectionViewCell {
    var show = true
    var fab = KCFloatingActionButton()
    @IBOutlet weak var imageZooomView: ImageZoomView!
    @IBOutlet weak var titleLabel: UILabel!
    private var sizes = Array<SizeModel>()
    override func awakeFromNib() {
        super.awakeFromNib()
         setUpFloatButton()
    }
    
    func showInfo() {
        show = !show
        if show {
            fab.alpha = 0
            UIView.animate(withDuration: 0.6, animations: { 
                self.fab.alpha = 1
            })
        } else {
            fab.alpha = 1
            UIView.animate(withDuration: 0.6, animations: {
                self.fab.alpha = 0
            })
        }
    }
    
    func setUpFloatButton() {
        fab.buttonColor = AppColor.MAIN_COLOR
        fab.openAnimationType = .fade
        fab.addItem("Download", icon: #imageLiteral(resourceName: "download").withRenderingMode(.alwaysTemplate)) { (button) in
            self.downloadImage()
        }
        fab.addItem("Copy image", icon: #imageLiteral(resourceName: "copy").withRenderingMode(.alwaysTemplate)) { (button) in
            self.copyImage()
        }
        fab.size = 65
        fab.itemSize = 55
        fab.rotationDegrees = 225
        fab.paddingX = 40
        fab.paddingY = 126
        fab.plusColor = UIColor.white
        self.insertSubview(fab, aboveSubview: self)
    }
    
    func copyImage() {
        guard let image = self.imageZooomView.zoomView?.image else { return }
        DispatchQueue.global().async {
            let pb = UIPasteboard.general
            let type = UIPasteboardTypeListImage[0] as! String
            if !type.isEmpty {
                pb.setData(UIImagePNGRepresentation(image)!, forPasteboardType: type)
                print("ok")
                DispatchQueue.main.async {
                    SVProgressHUD.showSuccess(withStatus: "Copied")
                }
            }
        }
        
    }
    
    func downloadImage() {
        canAccessPhotoLibrary(completion: { (success, error) in
            if success {
                SVProgressHUD.show()
                PHPhotoLibrary.shared().performChanges({
                    guard let image = self.imageZooomView.zoomView?.image else  { return }
                    PHAssetChangeRequest.creationRequestForAsset(from: image)
                }, completionHandler: { (success, error) in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.showDownloadResultAlert(success: success)
                    }
                    
                })
            }
        })
        
    }
    
    fileprivate func canAccessPhotoLibrary(completion: @escaping FLCompletion) {
        SVProgressHUD.show()
        var check = false
        PHPhotoLibrary.requestAuthorization { status in
            SVProgressHUD.dismiss()
            switch status {
            case .authorized:
                check = true
            case .restricted:
                DispatchQueue.main.async {
                    self.showAskPermissionAlert()
                }
                
                check = false
            case .denied:
                DispatchQueue.main.async {
                    self.showAskPermissionAlert()
                }
                check = false
            default:
                check = false
                break
            }
            completion(check, nil)
        }
    }
    
    func showDownloadResultAlert(success: Bool) {
        let alert = FCAlertView()
        success ? alert.makeAlertTypeSuccess() : alert.makeAlertTypeCaution()
        alert.showAlert(
                        withTitle: "Alert",
                        withSubtitle: success ? "Image downloaded" : "Please try again",
                        withCustomImage: nil,
                        withDoneButtonTitle: "Close",
                        andButtons: nil)
    }
    
    func showAskPermissionAlert() {
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.addButton("Okie") {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        }
        alert.showAlert(withTitle: AppString.APP_NAME,
                        withSubtitle: "Please allow this app to access camera roll so we can save photos to your library. 😜👌",
                        withCustomImage: nil,
                        withDoneButtonTitle: "Close",
                        andButtons: nil)
    }
    
    func setData(data: PhotoModel) {
        SVProgressHUD.show()
        self.titleLabel.text = data.title
        let service = RequestPhotoSizeService()
        service.requestPhotoSize(photoID: data.id, completion: { (data, error) in
            if let data = data {
                let sizeListModel = SizeListModel()
                sizeListModel.importJsonData(data: data)
                self.sizes = sizeListModel.sizes
                self.showImage()
            }
        })
    }
    
    func showImage() {
        for item: SizeModel in sizes where item.label == "Original" {
            let url = item.source
            let photoImageView = UIImageView(frame: self.frame)
            photoImageView.sd_setImage(with: URL(string: url)) { (image, error, cache, url) in
                DispatchQueue.main.async {
                    self.imageZooomView.alpha = 0
                    UIView.animate(withDuration: 0.5, animations: {
                        self.imageZooomView.display(image: photoImageView.image!)
                        self.imageZooomView.alpha = 1
                    })
                    SVProgressHUD.dismiss()
                }
            }
        }
    }

}

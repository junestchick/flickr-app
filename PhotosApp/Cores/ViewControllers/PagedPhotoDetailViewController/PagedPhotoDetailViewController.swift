//
//  PagedPhotoDetailViewController.swift
//  PhotosApp
//
//  Created by Jun on 4/13/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit

private let reuseIdentifier = "PagedPhotoDetailCollectionCell"

class PagedPhotoDetailViewController: UICollectionViewController {
    var photoArray = Array<PhotoModel>()
    var currentIndexPath = IndexPath()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateCollectionViewLayout(with: size)
    }
    
    private func updateCollectionViewLayout(with size: CGSize) {
        if let layout = collectionView!.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width: collectionView!.frame.size.width, height: collectionView!.frame.size.height)
            layout.invalidateLayout()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpUI()
    }
    
    func setUpUI() {
        self.collectionView!.indicatorStyle = .black
        self.collectionView!.scrollToItem(at: currentIndexPath, at: .centeredHorizontally, animated: false)
        let button = UIButton(frame: CGRect(x: ScreenSize.SCREEN_WIDTH - 106, y: ScreenSize.SCREEN_HEIGHT - 106, width: 66, height: 66))
        button.layer.cornerRadius = 33
        button.setTitle("X", for: .normal)
        button.layer.shadowOffset = CGSize(width: 1, height: 1)
        button.layer.shadowRadius = 2
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOpacity = 0.4
        button.backgroundColor = AppColor.MAIN_COLOR
        button.layer.shadowColor = UIColor.black.cgColor
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: Datasource
extension PagedPhotoDetailViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PagedPhotoDetailCollectionCell
                cell.setData(data: photoArray[indexPath.row])
        return cell
    }
}

// MARK: Layout delegate
extension PagedPhotoDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        return self.view.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumLineSpacingForSectionAt: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt: Int) -> CGFloat {
        return 0
    }
    
//    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
////        for cell in self.collectionView!.visibleCells {
////            currentIndexPath = self.collectionView!.indexPath(for: cell)!
////            break
////        }
//    }
}


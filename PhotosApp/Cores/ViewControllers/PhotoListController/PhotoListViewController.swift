//
//  PhotoListViewController.swift
//  PhotosApp
//
//  Created by Jun on 4/17/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SVProgressHUD
import FCAlertView
import GoogleMobileAds

private let reuseIdentifier = "PhotoListCollectionViewCell"

protocol PhotoListViewControllerDelegate: class {
    func photoListViewControllerDelegateDidClickGoHome()
}

class PhotoListViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerView: UIView!
    var bannerAds: GADBannerView!
    @IBOutlet weak var bannerHeightContraint: NSLayoutConstraint!
    var categoryID: String = ""
    var pageIndex = 1
    var pages = 1
    var total = 0
    var photoArray = Array<PhotoModel>()
    weak var delegate: PhotoListViewControllerDelegate?
    
        override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
            super.viewWillTransition(to: size, with: coordinator)
            updateCollectionViewLayout(with: size)
        }
    
    convenience init(categoryID: String, title: String) {
        self.init(nibName: "PhotoListViewController", bundle: nil)
        self.title = title
        self.categoryID = categoryID
    }
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadAds()
        if self.photoArray.count > 0 { return }
        if self.title == "SHUFFLE" {
            getShuffleData()
        } else {
            getData()
        }
    }
    
    private func updateCollectionViewLayout(with size: CGSize) {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.invalidateLayout()
        }
    }
    
    func setUpUI() {
        self.view.backgroundColor = UIColor.white
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView.register(UINib(nibName: "LoadMoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LoadMoreCollectionViewCell")
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.tintColor = AppColor.MAIN_COLOR
        let attributes = [ NSForegroundColorAttributeName : AppColor.MAIN_COLOR ] as [String: Any]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching new photos ...", attributes: attributes)
        refreshControl.addTarget(self, action: #selector(PhotoListViewController.refreshData), for: .valueChanged)
        
        //Add navi buttons
        self.addLeftBarButtonWithImage(#imageLiteral(resourceName: "list-menu").withRenderingMode(.alwaysTemplate))
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "home"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(PhotoListViewController.goHome))
        navigationItem.rightBarButtonItem = rightButton
        
        //Set ads
        
    }
    
    func goHome() {
        delegate?.photoListViewControllerDelegateDidClickGoHome()
    }
    
    func refreshData() {
        pageIndex = 1
        if self.title == "SHUFFLE" {
            getShuffleData()
        } else {
            getData()
        }
    }
    
    func loadAds() {
        bannerAds = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerAds.adUnitID = GoogleSDKKey.AD_UNIT_ID
        bannerAds.rootViewController = self
        bannerAds.delegate = self
        let req = GADRequest()
        #if DEBUG
            req.testDevices = [kGADSimulatorID]
        #endif
        bannerAds.load(req)
    }
    
    func shuffleKeywork() -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxy"
        let len = UInt32(letters.length)
        var randomString = ""
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
        return randomString
    }
    
    private func getShuffleData() {
        SVProgressHUD.show()
        let service = RequestSearchPhotoListService()
        let key = shuffleKeywork()
        service.requestSearchPhoto(keyword: key, atPage: pageIndex) { (data, error) in
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
            if let data = data {
                let photosModel = PhotoSearchListModel()
                photosModel.importJsonData(data: data)
                if photosModel.stat != "ok" {
                    self.showFailedAlert()
                } else {
                    self.photoArray.removeAll()
                    self.photoArray = photosModel.photos
                    self.pages = photosModel.pages
                    self.total = photosModel.total
                    self.loadItemsToFullScreen()
                    DispatchQueue.main.async {
                        self.collectionView.alpha = 0
                        self.collectionView.reloadData()
                        UIView.animate(withDuration: 1, animations: {
                            self.collectionView.alpha = 1
                        })
                    }
                }
                
            }
            
        }
    }
    
    private func getData() {
        SVProgressHUD.show()
        let service = RequestPhotoListInCategoryService()
        service.requestPhotoListInCategory(categoryID: categoryID, atPage: pageIndex) { [unowned self] (data, error) in
            self.refreshControl.endRefreshing()
            if let data = data {
                let photosModel = PhotoListModel()
                photosModel.importJsonData(data: data)
                if photosModel.stat != "ok" {
                    self.showFailedAlert()
                } else {
                    self.photoArray.removeAll()
                    self.photoArray = photosModel.photos
                    self.pages = photosModel.pages
                    self.total = photosModel.total
                    self.loadItemsToFullScreen()
                    DispatchQueue.main.async {
                        self.collectionView.alpha = 0
                        self.collectionView.reloadData()
                        UIView.animate(withDuration: 1, animations: {
                            self.collectionView.alpha = 1
                        })
                    }
                }
            }
            SVProgressHUD.dismiss()
        }
    }
    
    func loadItemsToFullScreen() {
        if pageIndex == pages &&  self.title != "SHUFFLE" { return }
        if photoArray.count < maximumItemsCanBeDisplayedAtOnce() {
            self.title == "SHUFFLE" ? loadShuffleMore() : loadMore()
        }
    }
    
    func maximumItemsCanBeDisplayedAtOnce() -> Int {
        let numberOfCol: CGFloat = CGFloat(GlobalConst.NUMBER_OF_COLUMNS)
        let collectionViewHeight = self.collectionView.frame.size.height
        let itemHeight = collectionView.frame.size.width / numberOfCol
        let maximumRows = Int(ceil(collectionViewHeight / itemHeight))
        return maximumRows * Int(numberOfCol)
    }
    
    func showFailedAlert() {
        let alert = FCAlertView()
        alert.makeAlertTypeWarning()
        alert.showAlert(withTitle: AppString.APP_NAME,
                        withSubtitle: "Failed to receive data. Give me a try. ☺️",
                        withCustomImage: nil,
                        withDoneButtonTitle: "Close",
                        andButtons: nil)
    }
    
}

// MARK: Admob Delegate
extension PhotoListViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
        bannerAds = bannerView
        if bannerHeightContraint.constant == 0 {
            UIView.animate(withDuration: 0.7) {
                self.bannerHeightContraint.constant = self.bannerAds.frame.size.height
                self.view.layoutIfNeeded()
                self.bannerAds.bounds = self.bannerView.frame
                self.bannerView.addSubview(self.bannerAds)
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        bannerHeightContraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    
}

// MARK: Scroll Delegate
extension PhotoListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height)
        {
            self.title == "SHUFFLE" ? loadShuffleMore() : loadMore()
            print("aaaaaaaaaa")
        }
    }
    
    func loadShuffleMore() {
        SVProgressHUD.show()
        let service = RequestSearchPhotoListService()
        let key = shuffleKeywork()
        service.requestSearchPhoto(keyword: key, atPage: pageIndex) { [unowned self] (data, error) in
            SVProgressHUD.dismiss()
            if let data = data {
                let photosModel = PhotoSearchListModel()
                photosModel.importJsonData(data: data)
                if photosModel.stat != "ok" {
                    self.showFailedAlert()
                } else {
                    self.total = photosModel.total
                    var newPhotos = photosModel.photos
                    for photo in newPhotos {
                        for old in self.photoArray {
                            if old.id == photo.id {
                                _ = newPhotos.remove(at: newPhotos.index(of: photo)!)
                                break
                            }
                        }
                    }
                    if newPhotos.count == 0 { return }
                    DispatchQueue.main.async {
                        self.collectionView!.performBatchUpdates({
                            var array = [IndexPath]()
                            let firstIndex = self.photoArray.count
                            for i in 0...newPhotos.count - 1 {
                                array.append(IndexPath(item: i + firstIndex, section: 0))
                            }
                            self.photoArray += newPhotos
                            self.collectionView.insertItems(at: array)
                        }, completion: { _ in
                            self.loadItemsToFullScreen()
                        })
                    }
                }
                
            }
            
        }
    }
    
    func loadMore() {
        pageIndex = pageIndex + 1
        if pageIndex > pages { return }
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        let service = RequestPhotoListInCategoryService()
        service.requestPhotoListInCategory(categoryID: categoryID, atPage: pageIndex) { [unowned self] (data, error) in
            SVProgressHUD.dismiss()
            if let data = data {
                let photosModel = PhotoListModel()
                photosModel.importJsonData(data: data)
                if photosModel.stat != "ok" {
                    self.showFailedAlert()
                } else {
                    self.total = photosModel.total
                    DispatchQueue.main.async {
                        self.collectionView!.performBatchUpdates({
                            var array = [IndexPath]()
                            let firstIndex = self.photoArray.count
                            for i in 0...photosModel.photos.count - 1 {
                                array.append(IndexPath(item: i + firstIndex, section: 0))
                            }
                            self.photoArray += photosModel.photos
                            self.collectionView.insertItems(at: array)
                        }, completion: { _ in
                            self.loadItemsToFullScreen()
                            })
                    }
                }
                
            }
            
        }
    }
}


// MARK: Datasource
extension PhotoListViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = NewPhotoDetailViewController(nibName: "NewPhotoDetailViewController", bundle: nil, photoModel: self.photoArray[indexPath.row], similarPhotos: photoArray)
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoListCollectionViewCell
        cell.setData(data: photoArray[indexPath.row])
        return cell
    }
    
}

// MARK: Layout delegate
extension PhotoListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        let numberOfCol: CGFloat = CGFloat(GlobalConst.NUMBER_OF_COLUMNS)
        return CGSize(width: collectionView.frame.size.width / numberOfCol, height: collectionView.frame.size.width / numberOfCol)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumLineSpacingForSectionAt: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt: Int) -> CGFloat {
        return 0
    }
}


//
//  PhotoListCollectionViewCell.swift
//  PhotosApp
//
//  Created by Jun on 4/12/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PhotoListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var overlayImageView: UIImageView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        NVActivityIndicatorView.DEFAULT_COLOR = AppColor.MAIN_COLOR
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        loadingIndicator.startAnimating()
    }
    
    func setData(data: PhotoModel) {
        let imageUrl = UIHelper.getPhotoLink(farm: data.farm, server: data.server, id: data.id, secret: data.secret, size: .small)
        overlayImageView.sd_setImage(with: URL(string: imageUrl)) { (image, error, cache, url) in
            self.loadingIndicator.stopAnimating()
            if cache == .none {
                self.overlayImageView.alpha = 0
                UIView.animate(withDuration: 1, animations: {
                    self.overlayImageView.alpha = 1
                })
            } else {
                self.overlayImageView.alpha = 1
            }
        }
    }

}

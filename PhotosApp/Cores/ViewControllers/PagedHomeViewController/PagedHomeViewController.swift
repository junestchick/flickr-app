//
//  PagedHomeViewController.swift
//  PhotosApp
//
//  Created by Jun on 4/14/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import PagingMenuController

protocol PagedHomeViewControllerDelegate: class {
    func pagedHomeViewController(didSearchKeyword keyword: String)
}

class PagedHomeViewController: BaseViewController {
    internal var pagingMenuController: PagingMenuController!
    weak var delegate: PagedHomeViewControllerDelegate?
    var searchBar: UISearchBar!
    var searchButton: UIBarButtonItem!
    var logoImageView: UIView?
    var contentViews = Array<UIView>()
    var contentLabels = [AppSegments.FIRST.NAME, AppSegments.SECOND.NAME, AppSegments.THIRD.NAME]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
//        pagingMenuController.view.layoutIfNeeded()
    }
    
    func setUpUI() {
        setUpNavi()
        setUpSearchBar()
        setUpPageMenu()
//        acTabSetup()
    }
    
    func setUpNavi() {
        self.addLeftBarButtonWithImage(#imageLiteral(resourceName: "list-menu").withRenderingMode(.alwaysTemplate))
        self.title = AppString.APP_NAME
        self.logoImageView = navigationItem.titleView
    }
    
    func setUpSearchBar() {
        //Search
        searchButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(showSearchBar))
        navigationItem.setRightBarButton(searchButton, animated: false)
        searchBar = UISearchBar()
        searchBar.delegate = self
        searchBar.setTextColor(color: UIColor.white)
        searchBar.searchBarStyle = .default
        searchBar.placeholder = "Search..."
        searchBar.showsCancelButton = true
    }
    
    func configureCancelBarButton() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(hideSearchBar))
        self.navigationItem.setRightBarButton(cancelButton, animated: true)
    }
    
    func setUpPageMenu() {
        let options = PagingMenuOptions()
        pagingMenuController = PagingMenuController(options: options)
//        pagingMenuController.view.frame.origin.y += 64
        pagingMenuController.view.frame.size.width = self.view.frame.size.width
        pagingMenuController.view.frame.size.height = self.view.frame.size.height
        print("KO XOAY", pagingMenuController.view.frame.size, self.view.frame.size)
        addChildViewController(pagingMenuController)
        view.addSubview(pagingMenuController.view)
        pagingMenuController.didMove(toParentViewController: self)
    }
    
}


//MARK: Search actions and delegate
extension PagedHomeViewController: UISearchBarDelegate {
    func searchAction() {
        delegate?.pagedHomeViewController(didSearchKeyword: "")
    }
    
    func showSearchBar() {
        DispatchQueue.main.async {
            self.navigationItem.titleView = self.searchBar
            self.navigationItem.setRightBarButton(nil, animated: true)
            if DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO {
                self.configureCancelBarButton()
            }
            self.navigationItem.titleView?.alpha = 0
            UIView.animate(withDuration: 0.6, animations: {
                self.navigationItem.titleView?.alpha = 1
                self.searchBar.becomeFirstResponder()
            }, completion: nil)
        }
        
    }
    
    func hideSearchBar() {
        DispatchQueue.main.async {
            self.searchBar.text = ""
            self.navigationItem.setRightBarButton(self.searchButton, animated: true)
            self.navigationItem.titleView?.alpha = 0
            self.navigationItem.titleView = self.logoImageView
            UIView.animate(withDuration: 0.3, animations: {
                self.navigationItem.titleView?.alpha = 1
            }, completion: { finished in
                
            })
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let keyword = searchBar.text else { return }
        hideSearchBar()
        let trueKeyword = keyword.trimmingCharacters(in: .whitespaces)
        let vc = SearchViewController(keyword: trueKeyword, title: "Search")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//
//  PagedHomeViewController+Config.swift
//  PhotosApp
//
//  Created by Jun on 4/14/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import Foundation
import PagingMenuController

extension PagedHomeViewController {
    
    internal struct PagingMenuOptions: PagingMenuControllerCustomizable {
        private let controller1 = PhotoListViewController(categoryID: AppSegments.FIRST.ID, title: AppSegments.FIRST.NAME)
        
        private let controller2 = PhotoListViewController(categoryID: AppSegments.SECOND.ID, title: AppSegments.SECOND.NAME)
        
        private let controller3 = PhotoListViewController(categoryID: AppSegments.THIRD.ID, title: AppSegments.THIRD.NAME)
        
        internal var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(), pagingControllers: pagingControllers)
        }
        
        internal var lazyLoadingPage: LazyLoadingPage {
            return .all
        }
        
        internal var pagingControllers: [UIViewController] {
            return [controller1 , controller2, controller3]
        }
        
        internal struct MenuOptions: MenuViewCustomizable {
            var displayMode: MenuDisplayMode {
                return .segmentedControl
            }
            
            var backgroundColor: UIColor {
                return AppColor.MAIN_COLOR.withAlphaComponent(0.1)
            }
            
            var selectedBackgroundColor: UIColor {
                return AppColor.MAIN_COLOR.withAlphaComponent(0.1)
            }
            
            var itemsOptions: [MenuItemViewCustomizable] {
                return [MenuItem1(), MenuItem2(), MenuItem3()]
            }
            
            var height: CGFloat {
                return 50
            }
            
            var animationDuration: TimeInterval {
                return 0.1
            }
            
            var focusMode: MenuFocusMode {
                return .underline(height: 4, color: AppColor.MAIN_COLOR, horizontalPadding: 0, verticalPadding: 0)
            }
        }
        
        internal struct MenuItem1: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText(text: AppSegments.FIRST.NAME, color: AppColor.MAIN_COLOR.withAlphaComponent(0.5), selectedColor: AppColor.MAIN_COLOR, font: UIFont.systemFont(ofSize: 17), selectedFont: UIFont.systemFont(ofSize: 18)))
            }
        }
        internal struct MenuItem2: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText(text: AppSegments.SECOND.NAME, color: AppColor.MAIN_COLOR.withAlphaComponent(0.5), selectedColor: AppColor.MAIN_COLOR, font: UIFont.systemFont(ofSize: 17), selectedFont: UIFont.systemFont(ofSize: 18)))
            }
        }
        
        internal struct MenuItem3: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                return .text(title: MenuItemText(text: AppSegments.THIRD.NAME, color: AppColor.MAIN_COLOR.withAlphaComponent(0.5), selectedColor: AppColor.MAIN_COLOR, font: UIFont.systemFont(ofSize: 17), selectedFont: UIFont.systemFont(ofSize: 18)))
            }
        }
    }

}

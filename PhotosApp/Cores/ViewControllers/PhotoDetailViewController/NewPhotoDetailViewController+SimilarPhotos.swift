//
//  NewPhotoDetailViewController+SimilarPhotos.swift
//  PhotosApp
//
//  Created by Jun on 5/4/17.
//  Copyright © 2017 Junest. All rights reserved.
//



extension NewPhotoDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.photoModel = self.similarPhotos[indexPath.row]
        loadPhoto()
//        let vc = NewPhotoDetailViewController(nibName: "NewPhotoDetailViewController", bundle: nil, photoModel: self.similarPhotos[indexPath.row], similarPhotos: similarPhotos)
//        vc.modalPresentationStyle = .custom
//        vc.modalTransitionStyle = .crossDissolve
//        self.present(vc, animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return similarPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoListCollectionViewCell", for: indexPath) as! PhotoListCollectionViewCell
        cell.setData(data: similarPhotos[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        let numberOfCol: CGFloat = CGFloat(GlobalConst.NUMBER_OF_COLUMNS)
        return CGSize(width: collectionView.frame.size.width / numberOfCol, height: collectionView.frame.size.width / numberOfCol)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumLineSpacingForSectionAt: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt: Int) -> CGFloat {
        return 0
    }
}

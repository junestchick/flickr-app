//
//  NewPhotoDetailViewController.swift
//  PhotosApp
//
//  Created by Jun on 4/26/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SVProgressHUD
import Photos
import SnapKit
import GoogleMobileAds
import SwiftyJSON
import FCAlertView
import Alamofire

private let HEIGHT_DOWNLOAD_VIEW: CGFloat = 100.0

class NewPhotoDetailViewController: BaseViewController {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var similarPhotosView: UICollectionView!
    var photoModel: PhotoModel?
    var similarPhotos: Array<PhotoModel>!
    
    var interstitial: GADInterstitial!
    @IBOutlet weak var mainPhotoImageView: UIImageView!
    @IBOutlet weak var imageHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!

//MARK: Overrides and init
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadPhoto()
    }

    convenience init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, photoModel: PhotoModel, similarPhotos: [PhotoModel]) {
        self.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.photoModel = photoModel
        self.title = photoModel.title
        self.similarPhotos = similarPhotos
    }
    
//MARK: Setup
    func setUpUI() {
        setZoomImage()
        setUpNavi()
        setUpButtonsView()
    }
    
    func setUpNavi() {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.dismissVC))
        self.similarPhotosView.register(UINib(nibName: "PhotoListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PhotoListCollectionViewCell")
        navigationItem.leftBarButtonItem = leftButton
    }
    
    func setUpButtonsView() {
        buttonsView.alpha = 0
        downloadButton.addTarget(self, action: #selector(self.downloadImage), for: .touchUpInside)
        copyButton.addTarget(self, action: #selector(self.copyImage), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(self.dismissVC), for: .touchUpInside)
        downloadButton.backgroundColor = AppColor.MAIN_COLOR
        downloadButton.setTitleColor(UIColor.white, for: .normal)
        copyButton.backgroundColor = AppColor.MAIN_COLOR
        copyButton.setTitleColor(UIColor.white, for: .normal)
        let theImageView = #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate)
        //        theImageView.co = AppColor.MAIN_COLOR
        backButton.setImage(theImageView, for: .normal)
        backButton.tintColor = AppColor.MAIN_COLOR
        UIHelper.setUIpropertyForView(view: downloadButton,
                                      borderColor: AppColor.MAIN_COLOR.cgColor,
                                      borderWidth: 0,
                                      cornerRadius: downloadButton.bounds.height / 2)
        UIHelper.setUIpropertyForView(view: copyButton,
                                      borderColor: AppColor.MAIN_COLOR.cgColor,
                                      borderWidth: 0,
                                      cornerRadius: copyButton.bounds.height / 2)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        buttonsView.roundCorners([.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 8)
        buttonsView.layer.masksToBounds = false
        buttonsView.layer.shadowColor = UIColor.red.cgColor
//        buttonsView.layer.shadowOpacity = 1
        buttonsView.layer.shadowOffset = CGSize(width: 10, height: 5)
        buttonsView.layer.shadowRadius = 100
        buttonsView.layer.shadowPath = UIBezierPath(rect: buttonsView.bounds).cgPath
    }
    
    
//MARK: Privare methods
    func dismissVC() {
        DispatchQueue.main.async {
            UIApplication.shared.isStatusBarHidden = false
            self.dismiss(animated: true, completion: { 
                
            })
        }
    }
    
    func setZoomImage() {
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchImage(gesture:)))
        self.view.addGestureRecognizer(pinch)
    }
    
    func pinchImage(gesture: UIPinchGestureRecognizer) {
        print(gesture.location(in: self.mainPhotoImageView))
        InstaImageZoom.shared.gestureStateChanged(gesture: gesture, withZoomImageView: self.mainPhotoImageView)
    }
    
    func downloadOriginalImage() {
        if let data = photoModel {
            SVProgressHUD.show()
            let service = RequestPhotoSizeService()
            service.requestPhotoSize(photoID: data.id, completion: { [unowned self](data, error) in
                guard let nonNilData = data else {
                    SVProgressHUD.showError(withStatus: "Download failed")
                    return
                }
                self.parseData(data: nonNilData)
            })
        }
    }
    
    func parseData(data: JSON) {
            let sizeListModel = SizeListModel()
            sizeListModel.importJsonData(data: data)
            for item: SizeModel in sizeListModel.sizes where item.label == "Original" {
                let url = item.source
                Alamofire.request(url).response(completionHandler: { (data) in
                    guard let noNilData = data.data, let image = UIImage(data: noNilData) else  {
                        SVProgressHUD.showError(withStatus: "Download failed")
                        return
                    }
                    self.saveImageToDisk(image: image)
                })
                
            }
    }
    
    func saveImageToDisk(image: UIImage) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAsset(from: image)
        }, completionHandler: { [unowned self] (success, error) in
            if success {
                self.sendTrackingAction(actionType: TrackingAction.download, value: self.photoModel!.id)
            }
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                success ? self.showInterstitialAds() :
                    SVProgressHUD.showError(withStatus: "Download failed")
            }
        })
    }
    
    func addLoadedImage(withSize size: CGSize) {
        let ratio = size.height / size.width;
        let trueWidth = self.contentView.frame.width;
        var newHeight = ratio * trueWidth;
        self.mainPhotoImageView.contentMode = .scaleAspectFit
        if newHeight > self.view.frame.height - HEIGHT_DOWNLOAD_VIEW {
           newHeight = self.view.frame.height - HEIGHT_DOWNLOAD_VIEW
        }
        self.imageHeightContraint.constant = newHeight
        self.contentView.layoutIfNeeded()
        self.mainPhotoImageView.alpha = 0
        UIView.animate(withDuration: 0.6) { [unowned self] in
            self.scrollView.layoutIfNeeded()
            self.scrollView.setContentOffset(CGPoint.zero, animated: true)
            self.mainPhotoImageView.alpha = 1
            self.buttonsView.alpha = 1
        }
    }
    
    func loadPhoto() {
        if let photoModel = photoModel {
            SVProgressHUD.show()
            self.titleLabel.text = photoModel.title
            let url = UIHelper.getPhotoLink(photoData: photoModel, size: .medium)
            mainPhotoImageView.sd_setImage(with: URL(string: url), completed: {[unowned self] (image, error, cache, url) in
                SVProgressHUD.dismiss()
                if let image = image {
                    self.addLoadedImage(withSize: image.size)
                    self.createAndLoadInterstitial()
                }
            })
        }
    }
    
    fileprivate func createAndLoadInterstitial() {
        interstitial = GADInterstitial(adUnitID: GoogleSDKKey.TEST_AD_ID)
        interstitial.delegate = self
        let request = GADRequest()
        #if DEBUG
            request.testDevices = [kGADSimulatorID]
        #endif
        interstitial.load(request)
    }
    
    func showInterstitialAds() {
        if interstitial.isReady {
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }
    
    fileprivate func canAccessPhotoLibrary(completion: @escaping FLCompletion) {
        SVProgressHUD.show()
        var check = false
        PHPhotoLibrary.requestAuthorization { status in
            SVProgressHUD.dismiss()
            switch status {
            case .authorized:
                check = true
            case .restricted:
                DispatchQueue.main.async {
                    self.showAskPermissionAlert()
                }
                check = false
            case .denied:
                DispatchQueue.main.async {
                    self.showAskPermissionAlert()
                }
                check = false
            default:
                check = false
                break
            }
            completion(check, nil)
        }
    }
    
    func copyImage() {
        guard let image = self.mainPhotoImageView.image else { return }
        DispatchQueue.global().async {
            let pb = UIPasteboard.general
            let type = UIPasteboardTypeListImage[0] as! String
            if !type.isEmpty {
                SVProgressHUD.show()
                DispatchQueue.global().async {
                    pb.setData(UIImagePNGRepresentation(image)!, forPasteboardType: type)
                    DispatchQueue.main.async {
                        SVProgressHUD.showSuccess(withStatus: "Copied")
                    }
                }
            }
        }
        
    }
    
    func downloadImage() {
        canAccessPhotoLibrary(completion: { (success, error) in
            if success {
                SVProgressHUD.show()
                PHPhotoLibrary.shared().performChanges({
                    guard let image = self.mainPhotoImageView.image else  { return }
                    PHAssetChangeRequest.creationRequestForAsset(from: image)
                }, completionHandler: { [unowned self] (success, error) in
                    if success { self.sendTrackingAction(actionType: TrackingAction.download, value: self.photoModel!.id)}
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        success ? self.showInterstitialAds() :
                             SVProgressHUD.showError(withStatus: "Download failed")
                    }
                    
                })
            }
        })
        
    }
    
    func showAskPermissionAlert() {
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.addButton("Close", withActionBlock: nil)
        alert.animateAlertInFromTop = true
        alert.doneActionBlock {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        }
        alert.showAlert(inView: self,
                        withTitle: "Alert",
                        withSubtitle: "Please allow this app to access camera roll so we can save photos to your library. 😜👌",
                        withCustomImage: nil,
                        withDoneButtonTitle: "Okie",
                        andButtons: nil)
    }
}

extension NewPhotoDetailViewController: GADInterstitialDelegate {
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        DispatchQueue.main.async {
            SVProgressHUD.showSuccess(withStatus: "Downloaded")
        }
        createAndLoadInterstitial()
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("OK ")
    }
    
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
    }
}


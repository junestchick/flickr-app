//
//  LeftMenuTableViewCell.swift
//  PhotosApp
//
//  Created by Jun on 4/12/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SDWebImage

class LeftMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        thumbnailImageView.layer.cornerRadius = 5
    }
    
    func setData(data: CategoryModel) {
        self.titleLabel.text = data.title
        self.countLabel.text = "\(data.photos)"
        let imageURL = UIHelper.getPhotoLink(farm: data.farm, server: data.server, id: data.primary, secret: data.secret, size: .small)
        self.thumbnailImageView.sd_setImage(with: URL(string: imageURL))
        self.backgroundColor = UIColor.clear
    }
}

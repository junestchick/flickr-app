//
//  LeftMenuViewController.swift
//  PhotosApp
//
//  Created by Jun on 4/12/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol LeftMenuViewControllerDelegate: class {
    func leftMenuViewController(didSelectItemWith categoryId: String, andTitle title: String)
}

class LeftMenuViewController: UIViewController {
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var overlayImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var refreshControl = UIRefreshControl()
    
    weak var delegate: LeftMenuViewControllerDelegate?
    var pageIndex = 1
    var categoryArray = Array<CategoryModel>()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        getData()
    }
    
    fileprivate func setUpUI() {
        categoryTableView.register(UINib(nibName: "LeftMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "LeftMenuTableViewCell")
        overlayImageView.image = UIImage(named: "welcome")
//        let dictionary = Bundle.main.infoDictionary!
//        let version = dictionary["CFBundleShortVersionString"] as! String
//        let build = dictionary["CFBundleVersion"] as! String
        nameLabel.text = AppString.APP_NAME
    if #available(iOS 10.0, *) {
    categoryTableView.refreshControl = refreshControl
    } else {
    categoryTableView.addSubview(refreshControl)
    }
    refreshControl.tintColor = AppColor.MAIN_COLOR
    let attributes = [ NSForegroundColorAttributeName : AppColor.MAIN_COLOR ] as [String: Any]
    refreshControl.attributedTitle = NSAttributedString(string: "Reload categories", attributes: attributes)
    refreshControl.addTarget(self, action: #selector(LeftMenuViewController.refreshData), for: .valueChanged)
    
}

    func refreshData() {
        pageIndex = 1
        getData()
    }

    fileprivate func getData() {
        SVProgressHUD.show()
        let service = RequestCategoryListService()
        service.requestCategoryList(atPage: pageIndex) { (data, error) in
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
            if let data = data {
                let categoryModel = CategoryListModel()
                categoryModel.importJsonData(data: data)
                for cate in categoryModel.categories {
                    if  cate.id != AppSegments.FIRST.ID &&
                        cate.id != AppSegments.SECOND.ID &&
                        cate.id != AppSegments.THIRD.ID {
                        self.categoryArray.append(cate)
                    }
                    
                }
                DispatchQueue.main.async {
                    self.categoryTableView.reloadData()
                }
            }
        }
    }
}

extension LeftMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            delegate?.leftMenuViewController(didSelectItemWith: "", andTitle: "")
            return
        }
        let item = self.categoryArray[indexPath.row - 1]
        DispatchQueue.main.async {
            self.delegate?.leftMenuViewController(didSelectItemWith: item.id, andTitle: item.title)
        }
        
    }
}

extension LeftMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableViewCell", for: indexPath) as! LeftMenuTableViewCell
        if indexPath.row == 0 {
            cell.titleLabel.text = "Home"
            cell.countLabel.text = ""
            cell.thumbnailImageView.image = UIImage(named: "home")
            cell.backgroundColor = UIColor.blue.withAlphaComponent(0.1)
            return cell
        }
        cell.setData(data: self.categoryArray[indexPath.row - 1])
        return cell
    }
}

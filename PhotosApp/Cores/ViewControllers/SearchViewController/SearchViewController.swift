//
//  SearchViewController.swift
//  PhotosApp
//
//  Created by Jun on 4/17/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import SVProgressHUD
import FCAlertView
import GoogleMobileAds

private let reuseIdentifier = "PhotoListCollectionViewCell"

class SearchViewController: BaseViewController {
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextfield: NewTextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerView: UIView!
    var bannerAds: GADBannerView!
    @IBOutlet weak var bannerHeightContraint: NSLayoutConstraint!
    var keyword: String = ""
    var pageIndex = 1
    var pages = 1
    var total = 0
    var photoArray = Array<PhotoModel>()
    weak var delegate: PhotoListViewControllerDelegate?
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateCollectionViewLayout(with: size)
    }
    
    convenience init(keyword: String, title: String) {
        self.init(nibName: "SearchViewController", bundle: nil)
        self.title = title
        self.keyword = keyword
    }
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadAds()
        if self.photoArray.count > 0 { return }
        getData()
    }
    
    //MARK: Private methods
    private func updateCollectionViewLayout(with size: CGSize) {
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let numberOfCol: CGFloat = CGFloat(GlobalConst.NUMBER_OF_COLUMNS)
            let sizeItem = CGSize(width: size.width / numberOfCol, height: size.width / numberOfCol)
            layout.itemSize = sizeItem
            layout.invalidateLayout()
        }
    }
    
    func showNoResultMessage(show: Bool) {
        if !show {
             self.collectionView.backgroundView = nil
            return
        }
        let rect = CGRect(x: 0,
                          y: 0,
                          width: self.collectionView.bounds.size.width,
                          height: self.collectionView.bounds.size.height)
        let noDataLabel: UILabel = UILabel(frame: rect)
        noDataLabel.font = UIFont.boldSystemFont(ofSize: 20)
        noDataLabel.text = "NO RESULT 😭"
        noDataLabel.textColor = UIColor.gray
        noDataLabel.textAlignment = NSTextAlignment.center
        self.collectionView.backgroundView = noDataLabel
    }
    
    @IBAction func searchAction() {
        guard let keyword = self.searchTextfield.text else { return }
        let trueKeyword = keyword.trimmingCharacters(in: .whitespaces)
        if trueKeyword.isEmpty { return }
        self.searchTextfield.resignFirstResponder()
        self.keyword = trueKeyword
        refreshData()
    }
    
    func setUpUI() {
        self.searchTextfield.text = keyword
        self.searchTextfield.enablesReturnKeyAutomatically = true
        self.view.backgroundColor = UIColor.white
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView.register(UINib(nibName: "LoadMoreCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LoadMoreCollectionViewCell")
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.tintColor = AppColor.MAIN_COLOR
        let attributes = [ NSForegroundColorAttributeName : AppColor.MAIN_COLOR ] as [String: Any]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching new photos ...", attributes: attributes)
        refreshControl.addTarget(self, action: #selector(SearchViewController.refreshData), for: .valueChanged)
        
        //Add navi buttons
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.dismissVC))
        navigationItem.leftBarButtonItem = leftButton
//        let rightButton: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "home"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(SearchViewController.goHome))
//        navigationItem.rightBarButtonItem = rightButtonf
    }
    
    func goHome() {
        delegate?.photoListViewControllerDelegateDidClickGoHome()
    }
    
    func dismissVC() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func refreshData() {
        pageIndex = 1
        getData()
    }
    
    func loadAds() {
        bannerAds = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerAds.adUnitID = GoogleSDKKey.AD_UNIT_ID
        bannerAds.rootViewController = self
        bannerAds.delegate = self
        let req = GADRequest()
        #if DEBUG
            req.testDevices = [kGADSimulatorID]
        #endif
        bannerAds.load(req)
    }
    
    private func getData() {
        SVProgressHUD.show()
        let service = RequestSearchPhotoListService()
        service.requestSearchPhoto(keyword: keyword, atPage: pageIndex) { (data, error) in
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
            if let data = data {
                let photosModel = PhotoSearchListModel()
                photosModel.importJsonData(data: data)
                if photosModel.stat != "ok" {
                    self.showFailedAlert()
                } else {
                    self.photoArray.removeAll()
                    self.photoArray = photosModel.photos
                    self.pages = photosModel.pages
                    self.total = photosModel.total
                    DispatchQueue.main.async {
                        self.collectionView.alpha = 0
                        self.collectionView.reloadData()
                        self.showNoResultMessage(show: self.total == 0)
                        UIView.animate(withDuration: 1, animations: {
                            self.collectionView.alpha = 1
                        })
                    }
                }
                
            }
            
        }
    }
    
    func loadMore() {
        pageIndex = pageIndex + 1
        if pageIndex > pages { return }
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        let service = RequestSearchPhotoListService()
        service.requestSearchPhoto(keyword: keyword, atPage: pageIndex) { (data, error) in
            SVProgressHUD.dismiss()
            if let data = data {
                let photosModel = PhotoSearchListModel()
                photosModel.importJsonData(data: data)
                if photosModel.stat != "ok" {
                    self.showFailedAlert()
                } else {
                    self.total = photosModel.total
                    DispatchQueue.main.async {
                        self.collectionView!.performBatchUpdates({
                            var array = [IndexPath]()
                            let firstIndex = self.photoArray.count
                            for i in 0...photosModel.photos.count - 1 {
                                array.append(IndexPath(item: i + firstIndex, section: 0))
                            }
                            self.photoArray += photosModel.photos
                            self.collectionView.insertItems(at: array)
                        }, completion: nil)
                    }
                }
                
            }
            
        }
    }
    
    func showFailedAlert() {
        let alert = FCAlertView()
        alert.makeAlertTypeWarning()
        alert.showAlert(withTitle: AppString.APP_NAME,
                        withSubtitle: "Failed to receive data. Give me a try. ☺️",
                        withCustomImage: nil,
                        withDoneButtonTitle: "Close",
                        andButtons: nil)
    }
    
}

extension SearchViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Banner loaded successfully")
        bannerAds = bannerView
        if bannerHeightContraint.constant == 0 {
            UIView.animate(withDuration: 0.7) {
                self.bannerHeightContraint.constant = self.bannerAds.frame.size.height
                self.view.layoutIfNeeded()
                self.bannerAds.bounds = self.bannerView.frame
                self.bannerView.addSubview(self.bannerAds)
            }
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        bannerHeightContraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    
}

// MARK: Scroll Delegate
extension SearchViewController {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offsetY = scrollView.contentOffset.y
        if offsetY <= 0 { return }
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            loadMore()
        }
    }
}

// MARK: Datasource
extension SearchViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = NewPhotoDetailViewController(nibName: "NewPhotoDetailViewController", bundle: nil, photoModel: self.photoArray[indexPath.row], similarPhotos: photoArray)
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationCapturesStatusBarAppearance = true
        self.present(vc, animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoListCollectionViewCell
        cell.setData(data: photoArray[indexPath.row])
        return cell
    }
}

// MARK: Layout delegate
extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        let numberOfCol: CGFloat = CGFloat(GlobalConst.NUMBER_OF_COLUMNS)
        return CGSize(width: collectionView.frame.size.width / numberOfCol, height: collectionView.frame.size.width / numberOfCol)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumLineSpacingForSectionAt: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt: Int) -> CGFloat {
        return 0
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .search {
            self.searchAction()
        }
        return true
    }
}

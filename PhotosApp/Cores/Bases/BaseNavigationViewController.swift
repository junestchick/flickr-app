//
//  BaseNavigationViewController.swift
//  HomeWork
//
//  Created by Dũng Trần on 12/8/16.
//  Copyright © 2016 Junest. All rights reserved.
//

import UIKit

class BaseNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    fileprivate func setup() {
        let textAttributes = [NSForegroundColorAttributeName:UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        UINavigationBar.appearance().tintColor = UIColor.white
        self.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "bgblur"), for: .default)
        if DeviceType.IS_IPAD {
            self.navigationBar.setBackgroundImage(UIImage.from(color: AppColor.MAIN_COLOR), for: .default)
        }
        navigationBar.contentMode = .scaleAspectFill
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = false
    }
}

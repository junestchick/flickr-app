//
//  BaseViewController.swift
//  PhotosApp
//
//  Created by Jun on 4/18/17.
//  Copyright © 2017 Junest. All rights reserved.
//

import UIKit
import FCAlertView

enum TrackKingCategory: String {
    case download   =   "Download"
    case search     =   "Search"
}

enum TrackingAction: String {
    case download   =   "Download"
    case search     =   "Search"
}


class BaseViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sendTrackingScreen()
    }
    
    func sendTrackingScreen() {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: self.title)
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func sendTrackingAction(actionType: TrackingAction, value: String) {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: self.title)
        
         guard let builder = GAIDictionaryBuilder.createEvent(withCategory: "Demo",
                                                        action: actionType.rawValue,
                                                        label: value,
                                                        value: 1) else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    
}

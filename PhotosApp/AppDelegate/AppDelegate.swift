//
//  AppDelegate.swift
//  homework
//
//  Created by Dũng Trần on 12/6/16.
//  Copyright © 2016 Junest. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import SVProgressHUD
import GoogleMobileAds
import FCAlertView
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainViewController: BaseNavigationViewController?
    var slideMenuController: SlideMenuController?
    var leftMenuViewController: UIViewController?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupWindow()
        setupSDKs()
        setUpMonitorNetwork()
        return true
    }

    fileprivate func getSlideMenuController() -> SlideMenuController {
        guard let nonNilSlideMenuController = slideMenuController else {
            SlideMenuOptions.contentViewScale = 1
            SlideMenuOptions.panGesturesEnabled = false
            if !(DeviceType.IS_IPAD || DeviceType.IS_IPAD_PRO) {
                SlideMenuOptions.leftViewWidth = ScreenSize.SCREEN_WIDTH * 8 / 10
            }
            let slideMenuController = SlideMenuController(mainViewController: getMainViewController(), leftMenuViewController: getLeftMenuViewController())
                self.slideMenuController = slideMenuController
            return slideMenuController
        }
        return nonNilSlideMenuController
    }
    
    fileprivate func getLeftMenuViewController() -> UIViewController {
        guard let noNilLeftMenuViewController = leftMenuViewController else {
            let leftMenuViewController = LeftMenuViewController(nibName: "LeftMenuViewController", bundle: nil)
            leftMenuViewController.delegate = self
            self.leftMenuViewController = leftMenuViewController
            return leftMenuViewController
        }
         return noNilLeftMenuViewController
    }
    
    fileprivate func getMainViewController() -> UIViewController {
        guard let noNilMainViewController = mainViewController else {
            let mainViewController = PagedHomeViewController(nibName: "PagedHomeViewController", bundle: nil)
            let rootNavigationViewController = BaseNavigationViewController(rootViewController: mainViewController)
            self.mainViewController = rootNavigationViewController
            return rootNavigationViewController
        }
        return noNilMainViewController
    }
    
    fileprivate func setupWindow() {
//        UIApplication.shared.isStatusBarHidden = true
        window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = getSlideMenuController()
        self.window?.makeKeyAndVisible()
    }
    
    fileprivate func setUpMonitorNetwork() {
        let reachability = Reachability()!
        
        reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                if reachability.isReachableViaWiFi {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
            }
        }
        reachability.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                let alert = FCAlertView()
                alert.makeAlertTypeCaution()
                alert.animateAlertInFromTop = true
                alert.showAlert(in: self.window!,
                                withTitle: "Alert",
                                withSubtitle: "Please allow this app to access internet. 😜👌",
                                withCustomImage: nil,
                                withDoneButtonTitle: "Okie",
                                andButtons: nil)
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    fileprivate func setupSDKs() {
        Fabric.with([Crashlytics.self])
        GADMobileAds.configure(withApplicationID: GoogleSDKKey.APP_ID)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setMinimumDismissTimeInterval(1.5)
        SVProgressHUD.setMinimumSize(CGSize(width: 50, height: 50))
        
        //set up Google Anaylytic 
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
        // Optional: configure GAI options.
        guard let gai = GAI.sharedInstance() else {
            return
        }
        gai.trackUncaughtExceptions = true
        #if DEBUG
            gai.logger.logLevel = GAILogLevel.verbose
        #endif
    }
    
}


extension AppDelegate: LeftMenuViewControllerDelegate {
    func leftMenuViewController(didSelectItemWith categoryId: String, andTitle title: String) {
        //TODO:
        if categoryId.isEmpty && title.isEmpty {
            self.slideMenuController?.changeMainViewController(getMainViewController(), close: true)
            return
        }
        let cateVC = PhotoListViewController(categoryID: categoryId, title: title)
        cateVC.delegate = self
        let navi = BaseNavigationViewController(rootViewController: cateVC)
        self.slideMenuController?.changeMainViewController(navi, close: true)
    }
}

extension AppDelegate: PhotoListViewControllerDelegate {
    func photoListViewControllerDelegateDidClickGoHome() {
        self.slideMenuController?.changeMainViewController(getMainViewController(), close: true)
    }
}

